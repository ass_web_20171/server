<?php
namespace App\Core;

class Router
{
    private $controller;
    private $action;
    private $request;
    public function __construct($request)
    {
        $this->request=$request;
        $this->prepareController();
        $this->prepareAction();
    }
    private function upperFisrt(&$str)
    {
        $str[0]=strtoupper($str[0]);
        return $str;
    }
    private function prepareController()
    {
        if ($this->request['controller']=='') {
            $this->controller="Home";
        } else {
            $this->controller=$this->request['controller'];
            $this->upperFisrt($this->controller);
        }
        if ($this->request['role']=="admin") {
            $this->controller="App\\Controllers\\Admin\\".$this->controller."Controller";
        } else {
            $this->controller="App\\Controllers\\User\\".$this->controller."Controller";
        }
    }
    private function prepareAction()
    {
        if ($this->request['action']=='') {
            $this->action="GetIndex";
        } else {
            $action=strtolower($_SERVER["REQUEST_METHOD"]).$this->upperFisrt($this->request['action']);
            $this->action=$this->upperFisrt($action);
        }
    }
    public function createController()
    {
        if (class_exists($this->controller)) {
            $parent=class_parents($this->controller);
            if (in_array("App\\Core\\Controller", $parent)) {
                if (method_exists($this->controller, $this->action)) {
                    return new $this->controller($this->action, $this->request);
                } else {
                    die("Action not exists");
                }
            } else {
                die("Dont have controller");
            }
        } else {
            die("Dont have class");
        }
    }
}
