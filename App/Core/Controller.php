<?php
namespace App\Core;

use League\Plates\Engine;

abstract class Controller
{
    protected $request;
    protected $action;
    protected $template;
    protected $session;
    public function __construct($action, $request)
    {
        $this->session=$_SESSION;
        $this->action=$action;
        $this->request=$request;
        $this->template=new Engine($this->getViewFolder());
    }
    public function executeAction()
    {
        return $this->{$this->action}();
    }
    private function getViewFolder()
    {
        $class=get_class($this);
        $class=str_replace("Controllers", "Views", $class);
        $class=str_replace("\\", "/", $class);
        $class=str_replace("Controller", "", $class);
        return $class;
    }
    protected function View($view, array $viewModel = array())
    {
        echo $this->template->render($view, $viewModel);
    }
}
