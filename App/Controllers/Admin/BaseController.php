<?php
namespace App\Controllers\Admin;

use App\Core\Controller;

class BaseController extends Controller
{
    public function __construct($action, $request)
    {
        parent::__construct($action, $request);
        if (!isset($this->session["admin"])) {
            header("Location: ".ROOT_PATH."admin/login");
            die();
        }

          //  $this->template.addFolder("admin","")
    }
}
