<?php

namespace App\Models\Map;

use App\Models\Story;
use App\Models\StoryQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'Story' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class StoryTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'App.Models.Map.StoryTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'Story';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\App\\Models\\Story';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'App.Models.Story';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the Id field
     */
    const COL_ID = 'Story.Id';

    /**
     * the column name for the Title field
     */
    const COL_TITLE = 'Story.Title';

    /**
     * the column name for the MetaTitle field
     */
    const COL_METATITLE = 'Story.MetaTitle';

    /**
     * the column name for the Description field
     */
    const COL_DESCRIPTION = 'Story.Description';

    /**
     * the column name for the AuthorId field
     */
    const COL_AUTHORID = 'Story.AuthorId';

    /**
     * the column name for the Thumbnail field
     */
    const COL_THUMBNAIL = 'Story.Thumbnail';

    /**
     * the column name for the Status field
     */
    const COL_STATUS = 'Story.Status';

    /**
     * the column name for the ReadCount field
     */
    const COL_READCOUNT = 'Story.ReadCount';

    /**
     * the column name for the PublishedDate field
     */
    const COL_PUBLISHEDDATE = 'Story.PublishedDate';

    /**
     * the column name for the CreatedBy field
     */
    const COL_CREATEDBY = 'Story.CreatedBy';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'Metatitle', 'Description', 'Authorid', 'Thumbnail', 'Status', 'Readcount', 'Publisheddate', 'Createdby', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'metatitle', 'description', 'authorid', 'thumbnail', 'status', 'readcount', 'publisheddate', 'createdby', ),
        self::TYPE_COLNAME       => array(StoryTableMap::COL_ID, StoryTableMap::COL_TITLE, StoryTableMap::COL_METATITLE, StoryTableMap::COL_DESCRIPTION, StoryTableMap::COL_AUTHORID, StoryTableMap::COL_THUMBNAIL, StoryTableMap::COL_STATUS, StoryTableMap::COL_READCOUNT, StoryTableMap::COL_PUBLISHEDDATE, StoryTableMap::COL_CREATEDBY, ),
        self::TYPE_FIELDNAME     => array('Id', 'Title', 'MetaTitle', 'Description', 'AuthorId', 'Thumbnail', 'Status', 'ReadCount', 'PublishedDate', 'CreatedBy', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'Metatitle' => 2, 'Description' => 3, 'Authorid' => 4, 'Thumbnail' => 5, 'Status' => 6, 'Readcount' => 7, 'Publisheddate' => 8, 'Createdby' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'metatitle' => 2, 'description' => 3, 'authorid' => 4, 'thumbnail' => 5, 'status' => 6, 'readcount' => 7, 'publisheddate' => 8, 'createdby' => 9, ),
        self::TYPE_COLNAME       => array(StoryTableMap::COL_ID => 0, StoryTableMap::COL_TITLE => 1, StoryTableMap::COL_METATITLE => 2, StoryTableMap::COL_DESCRIPTION => 3, StoryTableMap::COL_AUTHORID => 4, StoryTableMap::COL_THUMBNAIL => 5, StoryTableMap::COL_STATUS => 6, StoryTableMap::COL_READCOUNT => 7, StoryTableMap::COL_PUBLISHEDDATE => 8, StoryTableMap::COL_CREATEDBY => 9, ),
        self::TYPE_FIELDNAME     => array('Id' => 0, 'Title' => 1, 'MetaTitle' => 2, 'Description' => 3, 'AuthorId' => 4, 'Thumbnail' => 5, 'Status' => 6, 'ReadCount' => 7, 'PublishedDate' => 8, 'CreatedBy' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Story');
        $this->setPhpName('Story');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\App\\Models\\Story');
        $this->setPackage('App.Models');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('Id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('Title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('MetaTitle', 'Metatitle', 'VARCHAR', true, 255, null);
        $this->addColumn('Description', 'Description', 'VARCHAR', false, 255, null);
        $this->addForeignKey('AuthorId', 'Authorid', 'INTEGER', 'Author', 'Id', false, null, null);
        $this->addColumn('Thumbnail', 'Thumbnail', 'VARCHAR', false, 255, null);
        $this->addColumn('Status', 'Status', 'INTEGER', false, null, null);
        $this->addColumn('ReadCount', 'Readcount', 'INTEGER', false, null, null);
        $this->addColumn('PublishedDate', 'Publisheddate', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('CreatedBy', 'Createdby', 'INTEGER', 'User', 'Id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Author', '\\App\\Models\\Author', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':AuthorId',
    1 => ':Id',
  ),
), null, null, null, false);
        $this->addRelation('User', '\\App\\Models\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':CreatedBy',
    1 => ':Id',
  ),
), null, null, null, false);
        $this->addRelation('Categorymapping', '\\App\\Models\\Categorymapping', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':StoryId',
    1 => ':Id',
  ),
), null, null, 'Categorymappings', false);
        $this->addRelation('Chapter', '\\App\\Models\\Chapter', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':StoryId',
    1 => ':Id',
  ),
), null, null, 'Chapters', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? StoryTableMap::CLASS_DEFAULT : StoryTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Story object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = StoryTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = StoryTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + StoryTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = StoryTableMap::OM_CLASS;
            /** @var Story $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            StoryTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = StoryTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = StoryTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Story $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                StoryTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(StoryTableMap::COL_ID);
            $criteria->addSelectColumn(StoryTableMap::COL_TITLE);
            $criteria->addSelectColumn(StoryTableMap::COL_METATITLE);
            $criteria->addSelectColumn(StoryTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(StoryTableMap::COL_AUTHORID);
            $criteria->addSelectColumn(StoryTableMap::COL_THUMBNAIL);
            $criteria->addSelectColumn(StoryTableMap::COL_STATUS);
            $criteria->addSelectColumn(StoryTableMap::COL_READCOUNT);
            $criteria->addSelectColumn(StoryTableMap::COL_PUBLISHEDDATE);
            $criteria->addSelectColumn(StoryTableMap::COL_CREATEDBY);
        } else {
            $criteria->addSelectColumn($alias . '.Id');
            $criteria->addSelectColumn($alias . '.Title');
            $criteria->addSelectColumn($alias . '.MetaTitle');
            $criteria->addSelectColumn($alias . '.Description');
            $criteria->addSelectColumn($alias . '.AuthorId');
            $criteria->addSelectColumn($alias . '.Thumbnail');
            $criteria->addSelectColumn($alias . '.Status');
            $criteria->addSelectColumn($alias . '.ReadCount');
            $criteria->addSelectColumn($alias . '.PublishedDate');
            $criteria->addSelectColumn($alias . '.CreatedBy');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(StoryTableMap::DATABASE_NAME)->getTable(StoryTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(StoryTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(StoryTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new StoryTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Story or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Story object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \App\Models\Story) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(StoryTableMap::DATABASE_NAME);
            $criteria->add(StoryTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = StoryQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            StoryTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                StoryTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the Story table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return StoryQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Story or Criteria object.
     *
     * @param mixed               $criteria Criteria or Story object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Story object
        }

        if ($criteria->containsKey(StoryTableMap::COL_ID) && $criteria->keyContainsValue(StoryTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.StoryTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = StoryQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // StoryTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
StoryTableMap::buildTableMap();
