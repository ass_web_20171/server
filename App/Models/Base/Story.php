<?php

namespace App\Models\Base;

use \DateTime;
use \Exception;
use \PDO;
use App\Models\Author as ChildAuthor;
use App\Models\AuthorQuery as ChildAuthorQuery;
use App\Models\Categorymapping as ChildCategorymapping;
use App\Models\CategorymappingQuery as ChildCategorymappingQuery;
use App\Models\Chapter as ChildChapter;
use App\Models\ChapterQuery as ChildChapterQuery;
use App\Models\Story as ChildStory;
use App\Models\StoryQuery as ChildStoryQuery;
use App\Models\User as ChildUser;
use App\Models\UserQuery as ChildUserQuery;
use App\Models\Map\CategorymappingTableMap;
use App\Models\Map\ChapterTableMap;
use App\Models\Map\StoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'Story' table.
 *
 *
 *
 * @package    propel.generator.App.Models.Base
 */
abstract class Story implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\App\\Models\\Map\\StoryTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the metatitle field.
     *
     * @var        string
     */
    protected $metatitle;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the authorid field.
     *
     * @var        int
     */
    protected $authorid;

    /**
     * The value for the thumbnail field.
     *
     * @var        string
     */
    protected $thumbnail;

    /**
     * The value for the status field.
     *
     * @var        int
     */
    protected $status;

    /**
     * The value for the readcount field.
     *
     * @var        int
     */
    protected $readcount;

    /**
     * The value for the publisheddate field.
     *
     * @var        DateTime
     */
    protected $publisheddate;

    /**
     * The value for the createdby field.
     *
     * @var        int
     */
    protected $createdby;

    /**
     * @var        ChildAuthor
     */
    protected $aAuthor;

    /**
     * @var        ChildUser
     */
    protected $aUser;

    /**
     * @var        ObjectCollection|ChildCategorymapping[] Collection to store aggregation of ChildCategorymapping objects.
     */
    protected $collCategorymappings;
    protected $collCategorymappingsPartial;

    /**
     * @var        ObjectCollection|ChildChapter[] Collection to store aggregation of ChildChapter objects.
     */
    protected $collChapters;
    protected $collChaptersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCategorymapping[]
     */
    protected $categorymappingsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildChapter[]
     */
    protected $chaptersScheduledForDeletion = null;

    /**
     * Initializes internal state of App\Models\Base\Story object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Story</code> instance.  If
     * <code>obj</code> is an instance of <code>Story</code>, delegates to
     * <code>equals(Story)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Story The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [metatitle] column value.
     *
     * @return string
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [authorid] column value.
     *
     * @return int
     */
    public function getAuthorid()
    {
        return $this->authorid;
    }

    /**
     * Get the [thumbnail] column value.
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [readcount] column value.
     *
     * @return int
     */
    public function getReadcount()
    {
        return $this->readcount;
    }

    /**
     * Get the [optionally formatted] temporal [publisheddate] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPublisheddate($format = NULL)
    {
        if ($format === null) {
            return $this->publisheddate;
        } else {
            return $this->publisheddate instanceof \DateTimeInterface ? $this->publisheddate->format($format) : null;
        }
    }

    /**
     * Get the [createdby] column value.
     *
     * @return int
     */
    public function getCreatedby()
    {
        return $this->createdby;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[StoryTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [title] column.
     *
     * @param string $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[StoryTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [metatitle] column.
     *
     * @param string $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setMetatitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->metatitle !== $v) {
            $this->metatitle = $v;
            $this->modifiedColumns[StoryTableMap::COL_METATITLE] = true;
        }

        return $this;
    } // setMetatitle()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[StoryTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [authorid] column.
     *
     * @param int $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setAuthorid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->authorid !== $v) {
            $this->authorid = $v;
            $this->modifiedColumns[StoryTableMap::COL_AUTHORID] = true;
        }

        if ($this->aAuthor !== null && $this->aAuthor->getId() !== $v) {
            $this->aAuthor = null;
        }

        return $this;
    } // setAuthorid()

    /**
     * Set the value of [thumbnail] column.
     *
     * @param string $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setThumbnail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->thumbnail !== $v) {
            $this->thumbnail = $v;
            $this->modifiedColumns[StoryTableMap::COL_THUMBNAIL] = true;
        }

        return $this;
    } // setThumbnail()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[StoryTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [readcount] column.
     *
     * @param int $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setReadcount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->readcount !== $v) {
            $this->readcount = $v;
            $this->modifiedColumns[StoryTableMap::COL_READCOUNT] = true;
        }

        return $this;
    } // setReadcount()

    /**
     * Sets the value of [publisheddate] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setPublisheddate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->publisheddate !== null || $dt !== null) {
            if ($this->publisheddate === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->publisheddate->format("Y-m-d H:i:s.u")) {
                $this->publisheddate = $dt === null ? null : clone $dt;
                $this->modifiedColumns[StoryTableMap::COL_PUBLISHEDDATE] = true;
            }
        } // if either are not null

        return $this;
    } // setPublisheddate()

    /**
     * Set the value of [createdby] column.
     *
     * @param int $v new value
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function setCreatedby($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->createdby !== $v) {
            $this->createdby = $v;
            $this->modifiedColumns[StoryTableMap::COL_CREATEDBY] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    } // setCreatedby()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : StoryTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : StoryTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : StoryTableMap::translateFieldName('Metatitle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->metatitle = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : StoryTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : StoryTableMap::translateFieldName('Authorid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->authorid = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : StoryTableMap::translateFieldName('Thumbnail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->thumbnail = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : StoryTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : StoryTableMap::translateFieldName('Readcount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->readcount = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : StoryTableMap::translateFieldName('Publisheddate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->publisheddate = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : StoryTableMap::translateFieldName('Createdby', TableMap::TYPE_PHPNAME, $indexType)];
            $this->createdby = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = StoryTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\App\\Models\\Story'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aAuthor !== null && $this->authorid !== $this->aAuthor->getId()) {
            $this->aAuthor = null;
        }
        if ($this->aUser !== null && $this->createdby !== $this->aUser->getId()) {
            $this->aUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StoryTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildStoryQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aAuthor = null;
            $this->aUser = null;
            $this->collCategorymappings = null;

            $this->collChapters = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Story::setDeleted()
     * @see Story::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildStoryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                StoryTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAuthor !== null) {
                if ($this->aAuthor->isModified() || $this->aAuthor->isNew()) {
                    $affectedRows += $this->aAuthor->save($con);
                }
                $this->setAuthor($this->aAuthor);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->categorymappingsScheduledForDeletion !== null) {
                if (!$this->categorymappingsScheduledForDeletion->isEmpty()) {
                    \App\Models\CategorymappingQuery::create()
                        ->filterByPrimaryKeys($this->categorymappingsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->categorymappingsScheduledForDeletion = null;
                }
            }

            if ($this->collCategorymappings !== null) {
                foreach ($this->collCategorymappings as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->chaptersScheduledForDeletion !== null) {
                if (!$this->chaptersScheduledForDeletion->isEmpty()) {
                    foreach ($this->chaptersScheduledForDeletion as $chapter) {
                        // need to save related object because we set the relation to null
                        $chapter->save($con);
                    }
                    $this->chaptersScheduledForDeletion = null;
                }
            }

            if ($this->collChapters !== null) {
                foreach ($this->collChapters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[StoryTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . StoryTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(StoryTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'Id';
        }
        if ($this->isColumnModified(StoryTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'Title';
        }
        if ($this->isColumnModified(StoryTableMap::COL_METATITLE)) {
            $modifiedColumns[':p' . $index++]  = 'MetaTitle';
        }
        if ($this->isColumnModified(StoryTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'Description';
        }
        if ($this->isColumnModified(StoryTableMap::COL_AUTHORID)) {
            $modifiedColumns[':p' . $index++]  = 'AuthorId';
        }
        if ($this->isColumnModified(StoryTableMap::COL_THUMBNAIL)) {
            $modifiedColumns[':p' . $index++]  = 'Thumbnail';
        }
        if ($this->isColumnModified(StoryTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'Status';
        }
        if ($this->isColumnModified(StoryTableMap::COL_READCOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'ReadCount';
        }
        if ($this->isColumnModified(StoryTableMap::COL_PUBLISHEDDATE)) {
            $modifiedColumns[':p' . $index++]  = 'PublishedDate';
        }
        if ($this->isColumnModified(StoryTableMap::COL_CREATEDBY)) {
            $modifiedColumns[':p' . $index++]  = 'CreatedBy';
        }

        $sql = sprintf(
            'INSERT INTO Story (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'Id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'Title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'MetaTitle':
                        $stmt->bindValue($identifier, $this->metatitle, PDO::PARAM_STR);
                        break;
                    case 'Description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'AuthorId':
                        $stmt->bindValue($identifier, $this->authorid, PDO::PARAM_INT);
                        break;
                    case 'Thumbnail':
                        $stmt->bindValue($identifier, $this->thumbnail, PDO::PARAM_STR);
                        break;
                    case 'Status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case 'ReadCount':
                        $stmt->bindValue($identifier, $this->readcount, PDO::PARAM_INT);
                        break;
                    case 'PublishedDate':
                        $stmt->bindValue($identifier, $this->publisheddate ? $this->publisheddate->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'CreatedBy':
                        $stmt->bindValue($identifier, $this->createdby, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getMetatitle();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getAuthorid();
                break;
            case 5:
                return $this->getThumbnail();
                break;
            case 6:
                return $this->getStatus();
                break;
            case 7:
                return $this->getReadcount();
                break;
            case 8:
                return $this->getPublisheddate();
                break;
            case 9:
                return $this->getCreatedby();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Story'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Story'][$this->hashCode()] = true;
        $keys = StoryTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getMetatitle(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getAuthorid(),
            $keys[5] => $this->getThumbnail(),
            $keys[6] => $this->getStatus(),
            $keys[7] => $this->getReadcount(),
            $keys[8] => $this->getPublisheddate(),
            $keys[9] => $this->getCreatedby(),
        );
        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aAuthor) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'author';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Author';
                        break;
                    default:
                        $key = 'Author';
                }

                $result[$key] = $this->aAuthor->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'User';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCategorymappings) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'categorymappings';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'CategoryMappings';
                        break;
                    default:
                        $key = 'Categorymappings';
                }

                $result[$key] = $this->collCategorymappings->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collChapters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'chapters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'Chapters';
                        break;
                    default:
                        $key = 'Chapters';
                }

                $result[$key] = $this->collChapters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\App\Models\Story
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = StoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\App\Models\Story
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setMetatitle($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setAuthorid($value);
                break;
            case 5:
                $this->setThumbnail($value);
                break;
            case 6:
                $this->setStatus($value);
                break;
            case 7:
                $this->setReadcount($value);
                break;
            case 8:
                $this->setPublisheddate($value);
                break;
            case 9:
                $this->setCreatedby($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = StoryTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTitle($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMetatitle($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setAuthorid($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setThumbnail($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setStatus($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setReadcount($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPublisheddate($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCreatedby($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\App\Models\Story The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(StoryTableMap::DATABASE_NAME);

        if ($this->isColumnModified(StoryTableMap::COL_ID)) {
            $criteria->add(StoryTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(StoryTableMap::COL_TITLE)) {
            $criteria->add(StoryTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(StoryTableMap::COL_METATITLE)) {
            $criteria->add(StoryTableMap::COL_METATITLE, $this->metatitle);
        }
        if ($this->isColumnModified(StoryTableMap::COL_DESCRIPTION)) {
            $criteria->add(StoryTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(StoryTableMap::COL_AUTHORID)) {
            $criteria->add(StoryTableMap::COL_AUTHORID, $this->authorid);
        }
        if ($this->isColumnModified(StoryTableMap::COL_THUMBNAIL)) {
            $criteria->add(StoryTableMap::COL_THUMBNAIL, $this->thumbnail);
        }
        if ($this->isColumnModified(StoryTableMap::COL_STATUS)) {
            $criteria->add(StoryTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(StoryTableMap::COL_READCOUNT)) {
            $criteria->add(StoryTableMap::COL_READCOUNT, $this->readcount);
        }
        if ($this->isColumnModified(StoryTableMap::COL_PUBLISHEDDATE)) {
            $criteria->add(StoryTableMap::COL_PUBLISHEDDATE, $this->publisheddate);
        }
        if ($this->isColumnModified(StoryTableMap::COL_CREATEDBY)) {
            $criteria->add(StoryTableMap::COL_CREATEDBY, $this->createdby);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildStoryQuery::create();
        $criteria->add(StoryTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \App\Models\Story (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setMetatitle($this->getMetatitle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setAuthorid($this->getAuthorid());
        $copyObj->setThumbnail($this->getThumbnail());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setReadcount($this->getReadcount());
        $copyObj->setPublisheddate($this->getPublisheddate());
        $copyObj->setCreatedby($this->getCreatedby());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCategorymappings() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCategorymapping($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getChapters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addChapter($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \App\Models\Story Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildAuthor object.
     *
     * @param  ChildAuthor $v
     * @return $this|\App\Models\Story The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAuthor(ChildAuthor $v = null)
    {
        if ($v === null) {
            $this->setAuthorid(NULL);
        } else {
            $this->setAuthorid($v->getId());
        }

        $this->aAuthor = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildAuthor object, it will not be re-added.
        if ($v !== null) {
            $v->addStory($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildAuthor object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildAuthor The associated ChildAuthor object.
     * @throws PropelException
     */
    public function getAuthor(ConnectionInterface $con = null)
    {
        if ($this->aAuthor === null && ($this->authorid != 0)) {
            $this->aAuthor = ChildAuthorQuery::create()->findPk($this->authorid, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAuthor->addStories($this);
             */
        }

        return $this->aAuthor;
    }

    /**
     * Declares an association between this object and a ChildUser object.
     *
     * @param  ChildUser $v
     * @return $this|\App\Models\Story The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(ChildUser $v = null)
    {
        if ($v === null) {
            $this->setCreatedby(NULL);
        } else {
            $this->setCreatedby($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUser object, it will not be re-added.
        if ($v !== null) {
            $v->addStory($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUser object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUser The associated ChildUser object.
     * @throws PropelException
     */
    public function getUser(ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->createdby != 0)) {
            $this->aUser = ChildUserQuery::create()->findPk($this->createdby, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addStories($this);
             */
        }

        return $this->aUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Categorymapping' == $relationName) {
            $this->initCategorymappings();
            return;
        }
        if ('Chapter' == $relationName) {
            $this->initChapters();
            return;
        }
    }

    /**
     * Clears out the collCategorymappings collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCategorymappings()
     */
    public function clearCategorymappings()
    {
        $this->collCategorymappings = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCategorymappings collection loaded partially.
     */
    public function resetPartialCategorymappings($v = true)
    {
        $this->collCategorymappingsPartial = $v;
    }

    /**
     * Initializes the collCategorymappings collection.
     *
     * By default this just sets the collCategorymappings collection to an empty array (like clearcollCategorymappings());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCategorymappings($overrideExisting = true)
    {
        if (null !== $this->collCategorymappings && !$overrideExisting) {
            return;
        }

        $collectionClassName = CategorymappingTableMap::getTableMap()->getCollectionClassName();

        $this->collCategorymappings = new $collectionClassName;
        $this->collCategorymappings->setModel('\App\Models\Categorymapping');
    }

    /**
     * Gets an array of ChildCategorymapping objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildStory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCategorymapping[] List of ChildCategorymapping objects
     * @throws PropelException
     */
    public function getCategorymappings(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCategorymappingsPartial && !$this->isNew();
        if (null === $this->collCategorymappings || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCategorymappings) {
                // return empty collection
                $this->initCategorymappings();
            } else {
                $collCategorymappings = ChildCategorymappingQuery::create(null, $criteria)
                    ->filterByStory($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCategorymappingsPartial && count($collCategorymappings)) {
                        $this->initCategorymappings(false);

                        foreach ($collCategorymappings as $obj) {
                            if (false == $this->collCategorymappings->contains($obj)) {
                                $this->collCategorymappings->append($obj);
                            }
                        }

                        $this->collCategorymappingsPartial = true;
                    }

                    return $collCategorymappings;
                }

                if ($partial && $this->collCategorymappings) {
                    foreach ($this->collCategorymappings as $obj) {
                        if ($obj->isNew()) {
                            $collCategorymappings[] = $obj;
                        }
                    }
                }

                $this->collCategorymappings = $collCategorymappings;
                $this->collCategorymappingsPartial = false;
            }
        }

        return $this->collCategorymappings;
    }

    /**
     * Sets a collection of ChildCategorymapping objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $categorymappings A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildStory The current object (for fluent API support)
     */
    public function setCategorymappings(Collection $categorymappings, ConnectionInterface $con = null)
    {
        /** @var ChildCategorymapping[] $categorymappingsToDelete */
        $categorymappingsToDelete = $this->getCategorymappings(new Criteria(), $con)->diff($categorymappings);


        $this->categorymappingsScheduledForDeletion = $categorymappingsToDelete;

        foreach ($categorymappingsToDelete as $categorymappingRemoved) {
            $categorymappingRemoved->setStory(null);
        }

        $this->collCategorymappings = null;
        foreach ($categorymappings as $categorymapping) {
            $this->addCategorymapping($categorymapping);
        }

        $this->collCategorymappings = $categorymappings;
        $this->collCategorymappingsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Categorymapping objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Categorymapping objects.
     * @throws PropelException
     */
    public function countCategorymappings(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCategorymappingsPartial && !$this->isNew();
        if (null === $this->collCategorymappings || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCategorymappings) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCategorymappings());
            }

            $query = ChildCategorymappingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStory($this)
                ->count($con);
        }

        return count($this->collCategorymappings);
    }

    /**
     * Method called to associate a ChildCategorymapping object to this object
     * through the ChildCategorymapping foreign key attribute.
     *
     * @param  ChildCategorymapping $l ChildCategorymapping
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function addCategorymapping(ChildCategorymapping $l)
    {
        if ($this->collCategorymappings === null) {
            $this->initCategorymappings();
            $this->collCategorymappingsPartial = true;
        }

        if (!$this->collCategorymappings->contains($l)) {
            $this->doAddCategorymapping($l);

            if ($this->categorymappingsScheduledForDeletion and $this->categorymappingsScheduledForDeletion->contains($l)) {
                $this->categorymappingsScheduledForDeletion->remove($this->categorymappingsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCategorymapping $categorymapping The ChildCategorymapping object to add.
     */
    protected function doAddCategorymapping(ChildCategorymapping $categorymapping)
    {
        $this->collCategorymappings[]= $categorymapping;
        $categorymapping->setStory($this);
    }

    /**
     * @param  ChildCategorymapping $categorymapping The ChildCategorymapping object to remove.
     * @return $this|ChildStory The current object (for fluent API support)
     */
    public function removeCategorymapping(ChildCategorymapping $categorymapping)
    {
        if ($this->getCategorymappings()->contains($categorymapping)) {
            $pos = $this->collCategorymappings->search($categorymapping);
            $this->collCategorymappings->remove($pos);
            if (null === $this->categorymappingsScheduledForDeletion) {
                $this->categorymappingsScheduledForDeletion = clone $this->collCategorymappings;
                $this->categorymappingsScheduledForDeletion->clear();
            }
            $this->categorymappingsScheduledForDeletion[]= clone $categorymapping;
            $categorymapping->setStory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Story is new, it will return
     * an empty collection; or if this Story has previously
     * been saved, it will retrieve related Categorymappings from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Story.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCategorymapping[] List of ChildCategorymapping objects
     */
    public function getCategorymappingsJoinCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCategorymappingQuery::create(null, $criteria);
        $query->joinWith('Category', $joinBehavior);

        return $this->getCategorymappings($query, $con);
    }

    /**
     * Clears out the collChapters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addChapters()
     */
    public function clearChapters()
    {
        $this->collChapters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collChapters collection loaded partially.
     */
    public function resetPartialChapters($v = true)
    {
        $this->collChaptersPartial = $v;
    }

    /**
     * Initializes the collChapters collection.
     *
     * By default this just sets the collChapters collection to an empty array (like clearcollChapters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initChapters($overrideExisting = true)
    {
        if (null !== $this->collChapters && !$overrideExisting) {
            return;
        }

        $collectionClassName = ChapterTableMap::getTableMap()->getCollectionClassName();

        $this->collChapters = new $collectionClassName;
        $this->collChapters->setModel('\App\Models\Chapter');
    }

    /**
     * Gets an array of ChildChapter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildStory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildChapter[] List of ChildChapter objects
     * @throws PropelException
     */
    public function getChapters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collChaptersPartial && !$this->isNew();
        if (null === $this->collChapters || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collChapters) {
                // return empty collection
                $this->initChapters();
            } else {
                $collChapters = ChildChapterQuery::create(null, $criteria)
                    ->filterByStory($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collChaptersPartial && count($collChapters)) {
                        $this->initChapters(false);

                        foreach ($collChapters as $obj) {
                            if (false == $this->collChapters->contains($obj)) {
                                $this->collChapters->append($obj);
                            }
                        }

                        $this->collChaptersPartial = true;
                    }

                    return $collChapters;
                }

                if ($partial && $this->collChapters) {
                    foreach ($this->collChapters as $obj) {
                        if ($obj->isNew()) {
                            $collChapters[] = $obj;
                        }
                    }
                }

                $this->collChapters = $collChapters;
                $this->collChaptersPartial = false;
            }
        }

        return $this->collChapters;
    }

    /**
     * Sets a collection of ChildChapter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $chapters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildStory The current object (for fluent API support)
     */
    public function setChapters(Collection $chapters, ConnectionInterface $con = null)
    {
        /** @var ChildChapter[] $chaptersToDelete */
        $chaptersToDelete = $this->getChapters(new Criteria(), $con)->diff($chapters);


        $this->chaptersScheduledForDeletion = $chaptersToDelete;

        foreach ($chaptersToDelete as $chapterRemoved) {
            $chapterRemoved->setStory(null);
        }

        $this->collChapters = null;
        foreach ($chapters as $chapter) {
            $this->addChapter($chapter);
        }

        $this->collChapters = $chapters;
        $this->collChaptersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Chapter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Chapter objects.
     * @throws PropelException
     */
    public function countChapters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collChaptersPartial && !$this->isNew();
        if (null === $this->collChapters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collChapters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getChapters());
            }

            $query = ChildChapterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByStory($this)
                ->count($con);
        }

        return count($this->collChapters);
    }

    /**
     * Method called to associate a ChildChapter object to this object
     * through the ChildChapter foreign key attribute.
     *
     * @param  ChildChapter $l ChildChapter
     * @return $this|\App\Models\Story The current object (for fluent API support)
     */
    public function addChapter(ChildChapter $l)
    {
        if ($this->collChapters === null) {
            $this->initChapters();
            $this->collChaptersPartial = true;
        }

        if (!$this->collChapters->contains($l)) {
            $this->doAddChapter($l);

            if ($this->chaptersScheduledForDeletion and $this->chaptersScheduledForDeletion->contains($l)) {
                $this->chaptersScheduledForDeletion->remove($this->chaptersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildChapter $chapter The ChildChapter object to add.
     */
    protected function doAddChapter(ChildChapter $chapter)
    {
        $this->collChapters[]= $chapter;
        $chapter->setStory($this);
    }

    /**
     * @param  ChildChapter $chapter The ChildChapter object to remove.
     * @return $this|ChildStory The current object (for fluent API support)
     */
    public function removeChapter(ChildChapter $chapter)
    {
        if ($this->getChapters()->contains($chapter)) {
            $pos = $this->collChapters->search($chapter);
            $this->collChapters->remove($pos);
            if (null === $this->chaptersScheduledForDeletion) {
                $this->chaptersScheduledForDeletion = clone $this->collChapters;
                $this->chaptersScheduledForDeletion->clear();
            }
            $this->chaptersScheduledForDeletion[]= $chapter;
            $chapter->setStory(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aAuthor) {
            $this->aAuthor->removeStory($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removeStory($this);
        }
        $this->id = null;
        $this->title = null;
        $this->metatitle = null;
        $this->description = null;
        $this->authorid = null;
        $this->thumbnail = null;
        $this->status = null;
        $this->readcount = null;
        $this->publisheddate = null;
        $this->createdby = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCategorymappings) {
                foreach ($this->collCategorymappings as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collChapters) {
                foreach ($this->collChapters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCategorymappings = null;
        $this->collChapters = null;
        $this->aAuthor = null;
        $this->aUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(StoryTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
