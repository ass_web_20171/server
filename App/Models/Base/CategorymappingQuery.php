<?php

namespace App\Models\Base;

use \Exception;
use \PDO;
use App\Models\Categorymapping as ChildCategorymapping;
use App\Models\CategorymappingQuery as ChildCategorymappingQuery;
use App\Models\Map\CategorymappingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'CategoryMapping' table.
 *
 *
 *
 * @method     ChildCategorymappingQuery orderById($order = Criteria::ASC) Order by the Id column
 * @method     ChildCategorymappingQuery orderByCategoryid($order = Criteria::ASC) Order by the CategoryId column
 * @method     ChildCategorymappingQuery orderByStoryid($order = Criteria::ASC) Order by the StoryId column
 *
 * @method     ChildCategorymappingQuery groupById() Group by the Id column
 * @method     ChildCategorymappingQuery groupByCategoryid() Group by the CategoryId column
 * @method     ChildCategorymappingQuery groupByStoryid() Group by the StoryId column
 *
 * @method     ChildCategorymappingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCategorymappingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCategorymappingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCategorymappingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCategorymappingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCategorymappingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCategorymappingQuery leftJoinCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Category relation
 * @method     ChildCategorymappingQuery rightJoinCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Category relation
 * @method     ChildCategorymappingQuery innerJoinCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the Category relation
 *
 * @method     ChildCategorymappingQuery joinWithCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Category relation
 *
 * @method     ChildCategorymappingQuery leftJoinWithCategory() Adds a LEFT JOIN clause and with to the query using the Category relation
 * @method     ChildCategorymappingQuery rightJoinWithCategory() Adds a RIGHT JOIN clause and with to the query using the Category relation
 * @method     ChildCategorymappingQuery innerJoinWithCategory() Adds a INNER JOIN clause and with to the query using the Category relation
 *
 * @method     ChildCategorymappingQuery leftJoinStory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Story relation
 * @method     ChildCategorymappingQuery rightJoinStory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Story relation
 * @method     ChildCategorymappingQuery innerJoinStory($relationAlias = null) Adds a INNER JOIN clause to the query using the Story relation
 *
 * @method     ChildCategorymappingQuery joinWithStory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Story relation
 *
 * @method     ChildCategorymappingQuery leftJoinWithStory() Adds a LEFT JOIN clause and with to the query using the Story relation
 * @method     ChildCategorymappingQuery rightJoinWithStory() Adds a RIGHT JOIN clause and with to the query using the Story relation
 * @method     ChildCategorymappingQuery innerJoinWithStory() Adds a INNER JOIN clause and with to the query using the Story relation
 *
 * @method     \App\Models\CategoryQuery|\App\Models\StoryQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCategorymapping findOne(ConnectionInterface $con = null) Return the first ChildCategorymapping matching the query
 * @method     ChildCategorymapping findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCategorymapping matching the query, or a new ChildCategorymapping object populated from the query conditions when no match is found
 *
 * @method     ChildCategorymapping findOneById(int $Id) Return the first ChildCategorymapping filtered by the Id column
 * @method     ChildCategorymapping findOneByCategoryid(int $CategoryId) Return the first ChildCategorymapping filtered by the CategoryId column
 * @method     ChildCategorymapping findOneByStoryid(int $StoryId) Return the first ChildCategorymapping filtered by the StoryId column *

 * @method     ChildCategorymapping requirePk($key, ConnectionInterface $con = null) Return the ChildCategorymapping by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategorymapping requireOne(ConnectionInterface $con = null) Return the first ChildCategorymapping matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCategorymapping requireOneById(int $Id) Return the first ChildCategorymapping filtered by the Id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategorymapping requireOneByCategoryid(int $CategoryId) Return the first ChildCategorymapping filtered by the CategoryId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategorymapping requireOneByStoryid(int $StoryId) Return the first ChildCategorymapping filtered by the StoryId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCategorymapping[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCategorymapping objects based on current ModelCriteria
 * @method     ChildCategorymapping[]|ObjectCollection findById(int $Id) Return ChildCategorymapping objects filtered by the Id column
 * @method     ChildCategorymapping[]|ObjectCollection findByCategoryid(int $CategoryId) Return ChildCategorymapping objects filtered by the CategoryId column
 * @method     ChildCategorymapping[]|ObjectCollection findByStoryid(int $StoryId) Return ChildCategorymapping objects filtered by the StoryId column
 * @method     ChildCategorymapping[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CategorymappingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \App\Models\Base\CategorymappingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\App\\Models\\Categorymapping', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCategorymappingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCategorymappingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCategorymappingQuery) {
            return $criteria;
        }
        $query = new ChildCategorymappingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCategorymapping|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CategorymappingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CategorymappingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCategorymapping A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT Id, CategoryId, StoryId FROM CategoryMapping WHERE Id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCategorymapping $obj */
            $obj = new ChildCategorymapping();
            $obj->hydrate($row);
            CategorymappingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCategorymapping|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CategorymappingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CategorymappingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the Id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE Id = 1234
     * $query->filterById(array(12, 34)); // WHERE Id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE Id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategorymappingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the CategoryId column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryid(1234); // WHERE CategoryId = 1234
     * $query->filterByCategoryid(array(12, 34)); // WHERE CategoryId IN (12, 34)
     * $query->filterByCategoryid(array('min' => 12)); // WHERE CategoryId > 12
     * </code>
     *
     * @see       filterByCategory()
     *
     * @param     mixed $categoryid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByCategoryid($categoryid = null, $comparison = null)
    {
        if (is_array($categoryid)) {
            $useMinMax = false;
            if (isset($categoryid['min'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_CATEGORYID, $categoryid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryid['max'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_CATEGORYID, $categoryid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategorymappingTableMap::COL_CATEGORYID, $categoryid, $comparison);
    }

    /**
     * Filter the query on the StoryId column
     *
     * Example usage:
     * <code>
     * $query->filterByStoryid(1234); // WHERE StoryId = 1234
     * $query->filterByStoryid(array(12, 34)); // WHERE StoryId IN (12, 34)
     * $query->filterByStoryid(array('min' => 12)); // WHERE StoryId > 12
     * </code>
     *
     * @see       filterByStory()
     *
     * @param     mixed $storyid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByStoryid($storyid = null, $comparison = null)
    {
        if (is_array($storyid)) {
            $useMinMax = false;
            if (isset($storyid['min'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_STORYID, $storyid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($storyid['max'])) {
                $this->addUsingAlias(CategorymappingTableMap::COL_STORYID, $storyid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategorymappingTableMap::COL_STORYID, $storyid, $comparison);
    }

    /**
     * Filter the query by a related \App\Models\Category object
     *
     * @param \App\Models\Category|ObjectCollection $category The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByCategory($category, $comparison = null)
    {
        if ($category instanceof \App\Models\Category) {
            return $this
                ->addUsingAlias(CategorymappingTableMap::COL_CATEGORYID, $category->getId(), $comparison);
        } elseif ($category instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CategorymappingTableMap::COL_CATEGORYID, $category->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCategory() only accepts arguments of type \App\Models\Category or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Category relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function joinCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Category');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Category');
        }

        return $this;
    }

    /**
     * Use the Category relation Category object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\CategoryQuery A secondary query class using the current class as primary query
     */
    public function useCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Category', '\App\Models\CategoryQuery');
    }

    /**
     * Filter the query by a related \App\Models\Story object
     *
     * @param \App\Models\Story|ObjectCollection $story The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCategorymappingQuery The current query, for fluid interface
     */
    public function filterByStory($story, $comparison = null)
    {
        if ($story instanceof \App\Models\Story) {
            return $this
                ->addUsingAlias(CategorymappingTableMap::COL_STORYID, $story->getId(), $comparison);
        } elseif ($story instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CategorymappingTableMap::COL_STORYID, $story->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByStory() only accepts arguments of type \App\Models\Story or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Story relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function joinStory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Story');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Story');
        }

        return $this;
    }

    /**
     * Use the Story relation Story object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\StoryQuery A secondary query class using the current class as primary query
     */
    public function useStoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinStory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Story', '\App\Models\StoryQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCategorymapping $categorymapping Object to remove from the list of results
     *
     * @return $this|ChildCategorymappingQuery The current query, for fluid interface
     */
    public function prune($categorymapping = null)
    {
        if ($categorymapping) {
            $this->addUsingAlias(CategorymappingTableMap::COL_ID, $categorymapping->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the CategoryMapping table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategorymappingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CategorymappingTableMap::clearInstancePool();
            CategorymappingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategorymappingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CategorymappingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CategorymappingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CategorymappingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CategorymappingQuery
