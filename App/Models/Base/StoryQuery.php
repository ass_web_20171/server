<?php

namespace App\Models\Base;

use \Exception;
use \PDO;
use App\Models\Story as ChildStory;
use App\Models\StoryQuery as ChildStoryQuery;
use App\Models\Map\StoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'Story' table.
 *
 *
 *
 * @method     ChildStoryQuery orderById($order = Criteria::ASC) Order by the Id column
 * @method     ChildStoryQuery orderByTitle($order = Criteria::ASC) Order by the Title column
 * @method     ChildStoryQuery orderByMetatitle($order = Criteria::ASC) Order by the MetaTitle column
 * @method     ChildStoryQuery orderByDescription($order = Criteria::ASC) Order by the Description column
 * @method     ChildStoryQuery orderByAuthorid($order = Criteria::ASC) Order by the AuthorId column
 * @method     ChildStoryQuery orderByThumbnail($order = Criteria::ASC) Order by the Thumbnail column
 * @method     ChildStoryQuery orderByStatus($order = Criteria::ASC) Order by the Status column
 * @method     ChildStoryQuery orderByReadcount($order = Criteria::ASC) Order by the ReadCount column
 * @method     ChildStoryQuery orderByPublisheddate($order = Criteria::ASC) Order by the PublishedDate column
 * @method     ChildStoryQuery orderByCreatedby($order = Criteria::ASC) Order by the CreatedBy column
 *
 * @method     ChildStoryQuery groupById() Group by the Id column
 * @method     ChildStoryQuery groupByTitle() Group by the Title column
 * @method     ChildStoryQuery groupByMetatitle() Group by the MetaTitle column
 * @method     ChildStoryQuery groupByDescription() Group by the Description column
 * @method     ChildStoryQuery groupByAuthorid() Group by the AuthorId column
 * @method     ChildStoryQuery groupByThumbnail() Group by the Thumbnail column
 * @method     ChildStoryQuery groupByStatus() Group by the Status column
 * @method     ChildStoryQuery groupByReadcount() Group by the ReadCount column
 * @method     ChildStoryQuery groupByPublisheddate() Group by the PublishedDate column
 * @method     ChildStoryQuery groupByCreatedby() Group by the CreatedBy column
 *
 * @method     ChildStoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildStoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildStoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildStoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildStoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildStoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildStoryQuery leftJoinAuthor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Author relation
 * @method     ChildStoryQuery rightJoinAuthor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Author relation
 * @method     ChildStoryQuery innerJoinAuthor($relationAlias = null) Adds a INNER JOIN clause to the query using the Author relation
 *
 * @method     ChildStoryQuery joinWithAuthor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Author relation
 *
 * @method     ChildStoryQuery leftJoinWithAuthor() Adds a LEFT JOIN clause and with to the query using the Author relation
 * @method     ChildStoryQuery rightJoinWithAuthor() Adds a RIGHT JOIN clause and with to the query using the Author relation
 * @method     ChildStoryQuery innerJoinWithAuthor() Adds a INNER JOIN clause and with to the query using the Author relation
 *
 * @method     ChildStoryQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildStoryQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildStoryQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildStoryQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildStoryQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildStoryQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildStoryQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildStoryQuery leftJoinCategorymapping($relationAlias = null) Adds a LEFT JOIN clause to the query using the Categorymapping relation
 * @method     ChildStoryQuery rightJoinCategorymapping($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Categorymapping relation
 * @method     ChildStoryQuery innerJoinCategorymapping($relationAlias = null) Adds a INNER JOIN clause to the query using the Categorymapping relation
 *
 * @method     ChildStoryQuery joinWithCategorymapping($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Categorymapping relation
 *
 * @method     ChildStoryQuery leftJoinWithCategorymapping() Adds a LEFT JOIN clause and with to the query using the Categorymapping relation
 * @method     ChildStoryQuery rightJoinWithCategorymapping() Adds a RIGHT JOIN clause and with to the query using the Categorymapping relation
 * @method     ChildStoryQuery innerJoinWithCategorymapping() Adds a INNER JOIN clause and with to the query using the Categorymapping relation
 *
 * @method     ChildStoryQuery leftJoinChapter($relationAlias = null) Adds a LEFT JOIN clause to the query using the Chapter relation
 * @method     ChildStoryQuery rightJoinChapter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Chapter relation
 * @method     ChildStoryQuery innerJoinChapter($relationAlias = null) Adds a INNER JOIN clause to the query using the Chapter relation
 *
 * @method     ChildStoryQuery joinWithChapter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Chapter relation
 *
 * @method     ChildStoryQuery leftJoinWithChapter() Adds a LEFT JOIN clause and with to the query using the Chapter relation
 * @method     ChildStoryQuery rightJoinWithChapter() Adds a RIGHT JOIN clause and with to the query using the Chapter relation
 * @method     ChildStoryQuery innerJoinWithChapter() Adds a INNER JOIN clause and with to the query using the Chapter relation
 *
 * @method     \App\Models\AuthorQuery|\App\Models\UserQuery|\App\Models\CategorymappingQuery|\App\Models\ChapterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildStory findOne(ConnectionInterface $con = null) Return the first ChildStory matching the query
 * @method     ChildStory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildStory matching the query, or a new ChildStory object populated from the query conditions when no match is found
 *
 * @method     ChildStory findOneById(int $Id) Return the first ChildStory filtered by the Id column
 * @method     ChildStory findOneByTitle(string $Title) Return the first ChildStory filtered by the Title column
 * @method     ChildStory findOneByMetatitle(string $MetaTitle) Return the first ChildStory filtered by the MetaTitle column
 * @method     ChildStory findOneByDescription(string $Description) Return the first ChildStory filtered by the Description column
 * @method     ChildStory findOneByAuthorid(int $AuthorId) Return the first ChildStory filtered by the AuthorId column
 * @method     ChildStory findOneByThumbnail(string $Thumbnail) Return the first ChildStory filtered by the Thumbnail column
 * @method     ChildStory findOneByStatus(int $Status) Return the first ChildStory filtered by the Status column
 * @method     ChildStory findOneByReadcount(int $ReadCount) Return the first ChildStory filtered by the ReadCount column
 * @method     ChildStory findOneByPublisheddate(string $PublishedDate) Return the first ChildStory filtered by the PublishedDate column
 * @method     ChildStory findOneByCreatedby(int $CreatedBy) Return the first ChildStory filtered by the CreatedBy column *

 * @method     ChildStory requirePk($key, ConnectionInterface $con = null) Return the ChildStory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOne(ConnectionInterface $con = null) Return the first ChildStory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStory requireOneById(int $Id) Return the first ChildStory filtered by the Id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByTitle(string $Title) Return the first ChildStory filtered by the Title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByMetatitle(string $MetaTitle) Return the first ChildStory filtered by the MetaTitle column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByDescription(string $Description) Return the first ChildStory filtered by the Description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByAuthorid(int $AuthorId) Return the first ChildStory filtered by the AuthorId column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByThumbnail(string $Thumbnail) Return the first ChildStory filtered by the Thumbnail column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByStatus(int $Status) Return the first ChildStory filtered by the Status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByReadcount(int $ReadCount) Return the first ChildStory filtered by the ReadCount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByPublisheddate(string $PublishedDate) Return the first ChildStory filtered by the PublishedDate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildStory requireOneByCreatedby(int $CreatedBy) Return the first ChildStory filtered by the CreatedBy column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildStory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildStory objects based on current ModelCriteria
 * @method     ChildStory[]|ObjectCollection findById(int $Id) Return ChildStory objects filtered by the Id column
 * @method     ChildStory[]|ObjectCollection findByTitle(string $Title) Return ChildStory objects filtered by the Title column
 * @method     ChildStory[]|ObjectCollection findByMetatitle(string $MetaTitle) Return ChildStory objects filtered by the MetaTitle column
 * @method     ChildStory[]|ObjectCollection findByDescription(string $Description) Return ChildStory objects filtered by the Description column
 * @method     ChildStory[]|ObjectCollection findByAuthorid(int $AuthorId) Return ChildStory objects filtered by the AuthorId column
 * @method     ChildStory[]|ObjectCollection findByThumbnail(string $Thumbnail) Return ChildStory objects filtered by the Thumbnail column
 * @method     ChildStory[]|ObjectCollection findByStatus(int $Status) Return ChildStory objects filtered by the Status column
 * @method     ChildStory[]|ObjectCollection findByReadcount(int $ReadCount) Return ChildStory objects filtered by the ReadCount column
 * @method     ChildStory[]|ObjectCollection findByPublisheddate(string $PublishedDate) Return ChildStory objects filtered by the PublishedDate column
 * @method     ChildStory[]|ObjectCollection findByCreatedby(int $CreatedBy) Return ChildStory objects filtered by the CreatedBy column
 * @method     ChildStory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class StoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \App\Models\Base\StoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\App\\Models\\Story', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildStoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildStoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildStoryQuery) {
            return $criteria;
        }
        $query = new ChildStoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildStory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(StoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = StoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT Id, Title, MetaTitle, Description, AuthorId, Thumbnail, Status, ReadCount, PublishedDate, CreatedBy FROM Story WHERE Id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildStory $obj */
            $obj = new ChildStory();
            $obj->hydrate($row);
            StoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildStory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(StoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(StoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the Id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE Id = 1234
     * $query->filterById(array(12, 34)); // WHERE Id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE Id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the Title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE Title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE Title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the MetaTitle column
     *
     * Example usage:
     * <code>
     * $query->filterByMetatitle('fooValue');   // WHERE MetaTitle = 'fooValue'
     * $query->filterByMetatitle('%fooValue%', Criteria::LIKE); // WHERE MetaTitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metatitle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByMetatitle($metatitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metatitle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_METATITLE, $metatitle, $comparison);
    }

    /**
     * Filter the query on the Description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE Description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE Description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the AuthorId column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthorid(1234); // WHERE AuthorId = 1234
     * $query->filterByAuthorid(array(12, 34)); // WHERE AuthorId IN (12, 34)
     * $query->filterByAuthorid(array('min' => 12)); // WHERE AuthorId > 12
     * </code>
     *
     * @see       filterByAuthor()
     *
     * @param     mixed $authorid The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByAuthorid($authorid = null, $comparison = null)
    {
        if (is_array($authorid)) {
            $useMinMax = false;
            if (isset($authorid['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_AUTHORID, $authorid['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authorid['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_AUTHORID, $authorid['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_AUTHORID, $authorid, $comparison);
    }

    /**
     * Filter the query on the Thumbnail column
     *
     * Example usage:
     * <code>
     * $query->filterByThumbnail('fooValue');   // WHERE Thumbnail = 'fooValue'
     * $query->filterByThumbnail('%fooValue%', Criteria::LIKE); // WHERE Thumbnail LIKE '%fooValue%'
     * </code>
     *
     * @param     string $thumbnail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByThumbnail($thumbnail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($thumbnail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_THUMBNAIL, $thumbnail, $comparison);
    }

    /**
     * Filter the query on the Status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE Status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE Status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE Status > 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the ReadCount column
     *
     * Example usage:
     * <code>
     * $query->filterByReadcount(1234); // WHERE ReadCount = 1234
     * $query->filterByReadcount(array(12, 34)); // WHERE ReadCount IN (12, 34)
     * $query->filterByReadcount(array('min' => 12)); // WHERE ReadCount > 12
     * </code>
     *
     * @param     mixed $readcount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByReadcount($readcount = null, $comparison = null)
    {
        if (is_array($readcount)) {
            $useMinMax = false;
            if (isset($readcount['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_READCOUNT, $readcount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($readcount['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_READCOUNT, $readcount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_READCOUNT, $readcount, $comparison);
    }

    /**
     * Filter the query on the PublishedDate column
     *
     * Example usage:
     * <code>
     * $query->filterByPublisheddate('2011-03-14'); // WHERE PublishedDate = '2011-03-14'
     * $query->filterByPublisheddate('now'); // WHERE PublishedDate = '2011-03-14'
     * $query->filterByPublisheddate(array('max' => 'yesterday')); // WHERE PublishedDate > '2011-03-13'
     * </code>
     *
     * @param     mixed $publisheddate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByPublisheddate($publisheddate = null, $comparison = null)
    {
        if (is_array($publisheddate)) {
            $useMinMax = false;
            if (isset($publisheddate['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_PUBLISHEDDATE, $publisheddate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($publisheddate['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_PUBLISHEDDATE, $publisheddate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_PUBLISHEDDATE, $publisheddate, $comparison);
    }

    /**
     * Filter the query on the CreatedBy column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedby(1234); // WHERE CreatedBy = 1234
     * $query->filterByCreatedby(array(12, 34)); // WHERE CreatedBy IN (12, 34)
     * $query->filterByCreatedby(array('min' => 12)); // WHERE CreatedBy > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $createdby The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function filterByCreatedby($createdby = null, $comparison = null)
    {
        if (is_array($createdby)) {
            $useMinMax = false;
            if (isset($createdby['min'])) {
                $this->addUsingAlias(StoryTableMap::COL_CREATEDBY, $createdby['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdby['max'])) {
                $this->addUsingAlias(StoryTableMap::COL_CREATEDBY, $createdby['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(StoryTableMap::COL_CREATEDBY, $createdby, $comparison);
    }

    /**
     * Filter the query by a related \App\Models\Author object
     *
     * @param \App\Models\Author|ObjectCollection $author The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStoryQuery The current query, for fluid interface
     */
    public function filterByAuthor($author, $comparison = null)
    {
        if ($author instanceof \App\Models\Author) {
            return $this
                ->addUsingAlias(StoryTableMap::COL_AUTHORID, $author->getId(), $comparison);
        } elseif ($author instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StoryTableMap::COL_AUTHORID, $author->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAuthor() only accepts arguments of type \App\Models\Author or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Author relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function joinAuthor($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Author');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Author');
        }

        return $this;
    }

    /**
     * Use the Author relation Author object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\AuthorQuery A secondary query class using the current class as primary query
     */
    public function useAuthorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAuthor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Author', '\App\Models\AuthorQuery');
    }

    /**
     * Filter the query by a related \App\Models\User object
     *
     * @param \App\Models\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildStoryQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \App\Models\User) {
            return $this
                ->addUsingAlias(StoryTableMap::COL_CREATEDBY, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(StoryTableMap::COL_CREATEDBY, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \App\Models\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\App\Models\UserQuery');
    }

    /**
     * Filter the query by a related \App\Models\Categorymapping object
     *
     * @param \App\Models\Categorymapping|ObjectCollection $categorymapping the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStoryQuery The current query, for fluid interface
     */
    public function filterByCategorymapping($categorymapping, $comparison = null)
    {
        if ($categorymapping instanceof \App\Models\Categorymapping) {
            return $this
                ->addUsingAlias(StoryTableMap::COL_ID, $categorymapping->getStoryid(), $comparison);
        } elseif ($categorymapping instanceof ObjectCollection) {
            return $this
                ->useCategorymappingQuery()
                ->filterByPrimaryKeys($categorymapping->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCategorymapping() only accepts arguments of type \App\Models\Categorymapping or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Categorymapping relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function joinCategorymapping($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Categorymapping');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Categorymapping');
        }

        return $this;
    }

    /**
     * Use the Categorymapping relation Categorymapping object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\CategorymappingQuery A secondary query class using the current class as primary query
     */
    public function useCategorymappingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategorymapping($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Categorymapping', '\App\Models\CategorymappingQuery');
    }

    /**
     * Filter the query by a related \App\Models\Chapter object
     *
     * @param \App\Models\Chapter|ObjectCollection $chapter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildStoryQuery The current query, for fluid interface
     */
    public function filterByChapter($chapter, $comparison = null)
    {
        if ($chapter instanceof \App\Models\Chapter) {
            return $this
                ->addUsingAlias(StoryTableMap::COL_ID, $chapter->getStoryid(), $comparison);
        } elseif ($chapter instanceof ObjectCollection) {
            return $this
                ->useChapterQuery()
                ->filterByPrimaryKeys($chapter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByChapter() only accepts arguments of type \App\Models\Chapter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Chapter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function joinChapter($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Chapter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Chapter');
        }

        return $this;
    }

    /**
     * Use the Chapter relation Chapter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Models\ChapterQuery A secondary query class using the current class as primary query
     */
    public function useChapterQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChapter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Chapter', '\App\Models\ChapterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildStory $story Object to remove from the list of results
     *
     * @return $this|ChildStoryQuery The current query, for fluid interface
     */
    public function prune($story = null)
    {
        if ($story) {
            $this->addUsingAlias(StoryTableMap::COL_ID, $story->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the Story table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            StoryTableMap::clearInstancePool();
            StoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(StoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(StoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            StoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            StoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // StoryQuery
