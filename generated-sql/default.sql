
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- Author
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Author`;

CREATE TABLE `Author`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(255),
    PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Category`;

CREATE TABLE `Category`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(255),
    `MetaTitle` VARCHAR(255) NOT NULL,
    `Description` VARCHAR(255),
    PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- CategoryMapping
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `CategoryMapping`;

CREATE TABLE `CategoryMapping`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `CategoryId` INTEGER NOT NULL,
    `StoryId` INTEGER NOT NULL,
    PRIMARY KEY (`Id`),
    INDEX `fk_CategoryMapping_Category_1` (`CategoryId`),
    INDEX `fk_CategoryMapping_Story_1` (`StoryId`),
    CONSTRAINT `fk_CategoryMapping_Category_1`
        FOREIGN KEY (`CategoryId`)
        REFERENCES `Category` (`Id`),
    CONSTRAINT `fk_CategoryMapping_Story_1`
        FOREIGN KEY (`StoryId`)
        REFERENCES `Story` (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Chapter
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Chapter`;

CREATE TABLE `Chapter`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(255),
    `MetaTitle` VARCHAR(255),
    `Content` TEXT,
    `StoryId` INTEGER,
    `PublishedTime` DATETIME,
    `ReadCount` INTEGER,
    PRIMARY KEY (`Id`),
    INDEX `fk_Chapter_Story_1` (`StoryId`),
    CONSTRAINT `fk_Chapter_Story_1`
        FOREIGN KEY (`StoryId`)
        REFERENCES `Story` (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Role
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Role`;

CREATE TABLE `Role`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(255),
    `Description` VARCHAR(255),
    `Permission` INTEGER,
    PRIMARY KEY (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- Story
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `Story`;

CREATE TABLE `Story`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Title` VARCHAR(255) NOT NULL,
    `MetaTitle` VARCHAR(255) NOT NULL,
    `Description` VARCHAR(255),
    `AuthorId` INTEGER,
    `Thumbnail` VARCHAR(255),
    `Status` INTEGER,
    `ReadCount` INTEGER,
    `PublishedDate` DATETIME,
    `CreatedBy` INTEGER,
    PRIMARY KEY (`Id`),
    INDEX `fk_Story_Author_1` (`AuthorId`),
    INDEX `fk_Story_User_1` (`CreatedBy`),
    CONSTRAINT `fk_Story_Author_1`
        FOREIGN KEY (`AuthorId`)
        REFERENCES `Author` (`Id`),
    CONSTRAINT `fk_Story_User_1`
        FOREIGN KEY (`CreatedBy`)
        REFERENCES `User` (`Id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- User
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User`
(
    `Id` INTEGER NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(255) NOT NULL,
    `Username` VARCHAR(255) NOT NULL,
    `Password` VARCHAR(32) NOT NULL,
    `Gender` bit(1),
    `Email` VARCHAR(255) NOT NULL,
    `Phone` VARCHAR(20),
    `Birthday` DATETIME,
    `Avatar` VARCHAR(255),
    `RoleId` INTEGER,
    PRIMARY KEY (`Id`),
    INDEX `fk_User_Role_1` (`RoleId`),
    CONSTRAINT `fk_User_Role_1`
        FOREIGN KEY (`RoleId`)
        REFERENCES `Role` (`Id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
