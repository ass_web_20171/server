<?php
session_start();

require 'config.php';
require 'vendor/autoload.php';
require 'generated-conf/config.php';

use App\Core\Router;

$router=new Router($_GET);
$controller=$router->createController();
if ($controller) {
    $controller->executeAction();
}
